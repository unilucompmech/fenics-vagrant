**Note:** Virtual Machines and Docker images are now an official part of the FEniCS Project! Visit http://bitbucket.org/fenics-project/fenics-virtual for more information.

# Vagrant script to install Ubuntu with latest FEniCS Project and basic GUI

## Instructions

1. Install the virtual machine host [Virtualbox](https://www.virtualbox.org).
2. Install Vagrant, an automatic virtual machine host provisioner [Vagrant](http://www.vagrantup.com/).
3. Download this repository from [here](https://bitbucket.org/unilucompmech/fenics-vagrant/downloads) and extract it into e.g. `My Documents/dolfin`.
4. Open a terminal or command prompt (Windows: `cmd` in Start Menu)
5. Navigate to the folder you extracted to e.g. `cd 'My Documents/dolfin'`
6. The following command will automatically setup the virtual machine: `vagrant up`
7. Now restart the virtual machine: `vagrant reload`
8. This time the virtual machine will start with the GUI enabled.
9. The username is: `vagrant` and the password is: `vagrant`.
10. Open a terminal by right-clicking on the desktop, Applications > Terminal Emulators > XTerm (Unicode)
11. Try running ipython with the command `ipython` and importing dolfin with `import dolfin`.

## Tips and Tricks
* If you don't want to use the GUI change the `vb.gui` variable in Vagrantfile to `false`.
* Type `vagrant ssh` to start an SSH session.
* You don't need to edit your DOLFIN scripts in the virtual machine; the folder e.g. `My Documents/dolfin` is automatically shared with the virtual machine and is mounted at `/vagrant`. So you can run the script within the virtual machine, eg. `python /vagrant/my-dolfin-script.py` and do all your actual work with an editor/post-processor in Windows or OS X.